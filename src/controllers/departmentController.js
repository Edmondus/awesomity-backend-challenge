import models from '../database/models'
import * as employeeServices from "../services/employeeServices";

export default class Department{

    async createDepartment  (req, res) {
        const { name } = req.body;
    
        try {
            const isDepartmentExist = await employeeServices.departmentExist(name);
        
            if(isDepartmentExist) return res.status(409).json({status:409, message:"Department already exist"});
            const newDepartment = await models.Department.create({name})
            return res.status(201).json({status:201, message: 'Department created successful!', data: newDepartment});
            
        } catch (error) {
            return res.status(500).json({status:500, message: error.message});
        }
    
    }
    
     async allDepartment (req, res) {
        const departments = await models.Department.findAll()
        return res.status(200).json({status:200, message:"All department", departments})
    }
     async assignEmployeeToDepartment (req, res) {
        const {employee_code } = req.params
        const {department_id } = req.body
        try {
            const isEmployeeExist = await employeeServices.findEmployeeById
            (employee_code)
            console.log(">>>",isEmployeeExist);
            if(!isEmployeeExist) return res.status(404).json({status:404,message:"employee does not exist"});
    
             await models.Employee.update({department_id:department_id}, {where: {employee_code:isEmployeeExist.employee_code}})
            return res.status(200).json({status:200,message:"Employee assignged to department successful"})
        } catch (error) {
            return res.status(500).json({status:500, message: error.message});
        }
    }
}


