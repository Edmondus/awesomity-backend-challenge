
module.exports = (sequelize, DataTypes) => {
 const Department = sequelize.define('Department', {
  id:{
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
  },
    name: DataTypes.STRING,
  }, {});
  Department.associate = (models) => {
    Department.hasMany(models.Employee,{
      foreignKey: 'department_id',
      as: 'department',
    })
  }
  return Department;
};